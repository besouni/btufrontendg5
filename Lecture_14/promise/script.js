async function f() {
    let promise = new Promise(function(resolve, reject) {
        // setTimeout(
        //     () => resolve("done!"),
        //     1000
        // )

        var request = new XMLHttpRequest();
        request.open("GET", "../request/gallery.html")
        request.send();
        request.onload = function (){
            if(request.status == 200){
                // console.log(request.responseText);
                resolve(request.responseText);
            }else{
                document.getElementById("gallery").innerHTML = request.status;
            }
        }
    });

    // promise.then(
    //     function(value) {
    //         return value;
    //     }
    // );

    let result = await promise; // wait until the promise resolves (*)

    myDisplayer(result); // "done!"
}


function clickMe(){
    f();
}

// async function f() {
//     var x = 12;
//     x += 11;
//     console.log("hello");
//     return x;
// }
//
// function clickMe() {
//     f().then(function (v) {
//         console.log("Param "+v)
//     })
// }

// function clickMe(){
//     new Promise(function(resolve, reject) {
//
//         setTimeout(function() { resolve(1) }, 1000); // (*)
//
//     }).then(function(result) { // (**)
//
//         alert(result); // 1
//         return result * 4;
//
//     }).then(function(result) { // (***)
//
//         alert(result); // 2
//         return result * 2;
//
//     }).then(function(result) {
//
//         alert(result); // 4
//         return result - 5;
//
//     }).then(function(result) {
//         alert(result); // 4
//     })
// }


function myDisplayer(some) {
    document.getElementById("demo").innerHTML = some;
}
// function clickMe(){
//     // myDisplayer("Hello");
//     let myPromise = new Promise(function(resolve, myReject) {
//         let x = 0;
//
// // The producing code (this may take some time)
//
//         if (x == 0) {
//             resolve(89);
//         } else {
//             myReject("Error");
//         }
//     });
// //
//     myPromise.then(
//         function(v) {
//             myDisplayer((v));
//         },
//         function(error) {
//             myDisplayer(error);
//         }
//     );
// }
//



// let myPromise = new Promise(function(myResolve, myReject) {
// // "Producing Code" (May take some time)
//
//     myResolve(); // when successful
//     myReject();  // when error
// });
//
// // "Consuming Code" (Must wait for a fulfilled Promise)
// myPromise.then(
//     function(value) { /* code if successful */ },
//     function(error) { /* code if some error */ }
// );

// setTimeout(f1, 2000)
// setTimeout(function (){ console.log("Call Back") }, 2000)
//
// function f1(){
//     console.log("Hello")
// }
//
// function clickMe(){
//     f2(3, 4, sum)
// }
//
// function f2(n1, n2, callBack){
//     callBack(n1, n2);
// }
//
// function sum(a, b){
//     console.log(a+b);
// }