function show_cookies(){
    console.log(document.cookie)
}

function create_simple_cookie(){
    document.cookie = "uni=btu";
}

function create_cookie(){
    var name = document.getElementById("name").value;
    var value = document.querySelector("#value").value;
    var minutes = document.querySelector("#minutes").value;
    var d = new Date();
    console.log(d);
    console.log(d.getTime())
    d.setTime(d.getTime()+minutes*60*1000)
    console.log(d)
    // document.cookie = name+"="+value+"; expires="+d+"; path=/";
    document.cookie = name+"="+value+"; expires="+d+";";
}

function remove_cookie(){
    var name = document.getElementById("name").value;
    document.cookie = name + "=; Max-Age=0";
}

function create_storage(){
    var name = document.getElementById("name").value;
    var value = document.querySelector("#value").value;
    sessionStorage.setItem(name, value)
    localStorage.setItem(name, value)
}

function remove_storage(){
    var name = document.getElementById("name").value;
    sessionStorage.removeItem(name)
    localStorage.clear()
}