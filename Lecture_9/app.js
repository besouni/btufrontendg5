// console.log(document)

function f1(){
   var e1 = document.getElementById("p4");
   e1.innerText = "<em>HTML</em>"
   e2 = document.querySelector("#p3");
   e2.innerHTML = "<em>CSS</em>"
   e2.style.minHeight = "50px";
   e2.style.backgroundColor = "yellow";
   e2.style.color = "green"; 
}

// f1()

function f2(){
   n = document.getElementById("name").value;
   ptags = document.getElementsByClassName("p2");
   // ptags = document.querySelectorAll(".p2");
   console.log(ptags);
   ptags[0].style.backgroundColor  = "green";
   ptags[1].classList.add("example1");
   ptags['p1'].innerHTML = "GetElementByTagName";
   for(let i=0; i<ptags.length; i++){
      ptags[i].innerHTML = "<strong>"+n+"</strong>";
      ptags[i].style.color = "white";
   }
}

function f3(){
   d = document.getElementsByTagName("div");
   for(i=0; i<d.length; i++){
      d[i].classList.add("example2");
   }
}

function f4(){
   var d = document.getElementsByTagName("div");
   for(i=0; i<d.length; i++){
      d[i].classList.remove("example2");
   }
}